#include <reg51.h> 

sbit LS138A = P2^2;  	//定义138译码器的输入A脚由P2.2控制 
sbit LS138B = P2^3;	    //定义138译码器的输入脚B由P2.3控制
sbit LS138C = P2^4; 	//定义138译码器的输入脚C由P2.4控制

unsigned char code char_table[10] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f}; 

void delay(unsigned int i)；
unsigned long intpower(unsigned char number, unsigned char _power)；
void display_number(unsigned long number)；

void main()
{
	while(1) {
		unsigned long i;
		for(i = 1; i < 10000; i++) {
			display_number(intpower(2,i));
		}
		
	}

}

void delay(unsigned int i)
{

    while(i--);
	/*
    char j;
    for(i; i > 0; i--)
        for(j = 200; j > 0; j--);
	*/

}


unsigned long intpower(unsigned char number, unsigned char _power) {
	if(_power == 0) 
		return 1;
	else {
		unsigned char i;
		unsigned long product = 1;
		for(i = 1; i <= _power; i++) {
			product = product * number;
		}
		return product;
	}
}

void display_number(unsigned long number) {
	unsigned char digits[8];
	unsigned char i, j, k;
	
	for(i = 0; i < 8; i++) {
		digits[i] = (number%intpower(10, i+1)/intpower(10,i));
	}
	
	/*
	digits[0] = number%10;
	digits[1] = number%100/10;
	digits[2] = number%1000/100;
	digits[3] = number%10000/1000;
	digits[4] = number%100000/10000;
	digits[5] = number%1000000/100000;
	digits[6] = number%10000000/1000000;
	digits[6] = number%100000000/10000000;
	digits[7] = number%1000000000/100000000;
	*/
	// 假设light_digit中的每位灯点亮时间间隔为T，则对于每位灯都有：点亮T，熄灭8T（8位灯情况下），
	// 如果T太大，则8T太大，熄灭时间太长会造成闪烁感，可自行调试T的值观察效果；建议light_digit中延时在100us ~ 500us；
	// 显示完8位耗时 8T，若T = 200us, 则一个8位数停留显示时间只有1.6ms，1S 内则可以显示大约200个不同的数，变化率太快
	// 因此需要让每个8位数重复显示多次，直到显示停留时间够长，然后接下去显示另外一个数，如果需要的话；
	// light_digit中的延时决定了灯的点亮时间以及闪烁感，延时和重复次数综合决定了每个数的显示总时间或者说数字的变化速率；
	for(i = 1; i <= 20; i++) {
		for(j = 0; j < 8; j++) {
			P0 = char_table[digits[j]];
			switch(j) {
                case 0: LS138A=1; LS138B=1; LS138C=1;  break;
                case 1: LS138A=0; LS138B=1; LS138C=1;  break;
                case 2: LS138A=1; LS138B=0; LS138C=1;  break;
                case 3: LS138A=0; LS138B=0; LS138C=1;  break;
                case 4: LS138A=1; LS138B=1; LS138C=0;  break; 
                case 5: LS138A=0; LS138B=1; LS138C=0;  break;
                case 6: LS138A=1; LS138B=0; LS138C=0;  break; 
                case 7: LS138A=0; LS138B=0; LS138C=0;  break;
        	}
        
	
			// 延时让灯点亮一会儿;
			delay(200);
		}
	}
}