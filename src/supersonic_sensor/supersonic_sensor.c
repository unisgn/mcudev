/*
 * supersonic_sensor.c
 */
#include <reg51.h> 
#define SYS_FREQ 1000000
#define SONIC_SPEED 340
#define MAX_RANGE 4000 // max measure range of sensor, in mm;

// pins configuration
sbit ECHO = P3^2;
sbit TRIG = P3^0;
sbit LS138A = P2^2;     
sbit LS138B = P2^3;         
sbit LS138C = P2^4;     

//字符映射表"0-9"
unsigned char code char_table[10] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f}; 

void display_number(unsigned int number);
void light_digit(unsigned char digit, unsigned char _char);     
void delay(unsigned int i);
void sys_init();
void trigger_signal();
unsigned int measure_distance();
unsigned int intpower(unsigned char number, unsigned char _power);

void main()
{
        sys_init();
        while(1) {
                display_number(measure_distance());
                delay(200);
        }
}

void sys_init() {
        // GATE = 1, timer0 controlled by both TR0 and INT0; M1M0 = 01, timer function in mode 1;
        TMOD = 0x09; // 00001001B
}

void display_number(unsigned int number) {
        unsigned char j, k;
		
        // lighting 8-bit number frame for 10 times;
        for(j = 1; j <= 10; j++) {
                for(k = 0; k < 8; k++) {
                        light_digit(k, char_table[number%intpower(10, k+1)/intpower(10,k)]);
                }
        }
}

//light single digit;
void light_digit(unsigned char digit, unsigned char _char) {
        P0 = _char;
        
        // choose digit to light via LS138 routine
        switch(digit) {
            case 0: LS138A=1; LS138B=1; LS138C=1;  break;
            case 1: LS138A=0; LS138B=1; LS138C=1;  break;
            case 2: LS138A=1; LS138B=0; LS138C=1;  break;
            case 3: LS138A=0; LS138B=0; LS138C=1;  break;
            case 4: LS138A=1; LS138B=1; LS138C=0;  break; 
            case 5: LS138A=0; LS138B=1; LS138C=0;  break;
            case 6: LS138A=1; LS138B=0; LS138C=0;  break; 
            case 7: LS138A=0; LS138B=0; LS138C=0;  break;
        }
        
        // delay to make lighting last longer;
        delay(200);
}

void delay(unsigned int i) {

    while(i--);
        /*
    char j;
    for(i; i > 0; i--)
        for(j = 200; j > 0; j--);
        */
}

unsigned int measure_distance() {
        unsigned int distance;
        ECHO = 0;
        TH0 = 0;
        TL0 = 0;
        trigger_signal();
        TR0 = 1;
        
        // detect/waitting for ECHO turning to HIGH;
        while(!ECHO) { ; }
        
        // detect/waitting for ECHO turning to LOW;
        while(ECHO) { ; }

        TR0 = 0;
        /* system_freq = 11.0592MHz, sonic_speed = 340 m/s, 
		 * distance in mm = (256*TH0+TL0) * 12 / system_freq * sonic_speed /2 * 1000;
		 * = (256*TH0+TL0)*0.1845; 
		 */
        distance = (256 * TH0 + TL0) * 0.1845;          
        if(TF0 || distance >= MAX_RANGE)        // out of measure range;
			return 0;
        else
			return distance;
}

void trigger_signal() {
        TRIG = 1;
        delay(80);
        TRIG = 0;
}

unsigned int intpower(unsigned char number, unsigned char _power) {
        if(_power == 0) 
                return 1;
        else
                return number * intpower(number, _power-1);
}